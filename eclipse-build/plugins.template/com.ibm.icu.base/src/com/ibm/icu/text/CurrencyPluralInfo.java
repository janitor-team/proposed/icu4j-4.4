/*
 *******************************************************************************
 * Copyright (C) 2011, International Business Machines Corporation and         *
 * others. All Rights Reserved.                                                *
 *******************************************************************************
 */
package com.ibm.icu.text;

/*
 * Empty stub
 */
public class CurrencyPluralInfo {
    private CurrencyPluralInfo() {}
}
